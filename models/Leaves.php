<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "leaves".
 *
 * @property int $leave_ID
 * @property string $date
 * @property string $reason
 * @property int $emp_ID
 *
 * @property Employee $emp
 * @property Payroll[] $payrolls
 */
class Leaves extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leaves';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['leave_ID', 'date', 'reason', 'emp_ID'], 'required'],
            [['leave_ID', 'emp_ID'], 'integer'],
            [['date'], 'safe'],
            [['reason'], 'string', 'max' => 200],
            [['leave_ID'], 'unique'],
            [['emp_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['emp_ID' => 'emp_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'leave_ID' => 'Leave ID',
            'date' => 'Date',
            'reason' => 'Reason',
            'emp_ID' => 'Employee ID',
        ];
    }

    /**
     * Gets query for [[Emp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmp()
    {
        return $this->hasOne(Employee::className(), ['emp_ID' => 'emp_ID']);
    }

    /**
     * Gets query for [[Payrolls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayrolls()
    {
        return $this->hasMany(Payroll::className(), ['leave_ID' => 'leave_ID']);
    }
}
