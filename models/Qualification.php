<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "qualification".
 *
 * @property int $qual_ID
 * @property string $position
 * @property string $requirements
 * @property string $date_in
 * @property int $emp_ID
 *
 * @property Employee $emp
 */
class Qualification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qualification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'requirements', 'date_in', 'emp_ID'], 'required'],
            [['qual_ID', 'emp_ID'], 'integer'],
            [['date_in'], 'safe'],
            [['position'], 'string', 'max' => 80],
            [['requirements'], 'string', 'max' => 100],
            [['qual_ID'], 'unique'],
            [['emp_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['emp_ID' => 'emp_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'qual_ID' => 'Qualification ID',
            'position' => 'Position',
            'requirements' => 'Requirements',
            'date_in' => 'Date In',
            'emp_ID' => 'Employee ID',
        ];
    }

    /**
     * Gets query for [[Emp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmp()
    {
        return $this->hasOne(Employee::className(), ['emp_ID' => 'emp_ID']);
    }
}
