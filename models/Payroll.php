<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payroll".
 *
 * @property int $payroll_ID
 * @property string $date
 * @property string $report
 * @property int $total_amount
 * @property int $emp_ID
 * @property int $job_ID
 * @property int $salary_ID
 * @property int $leave_ID
 *
 * @property Employee $emp
 * @property JobDepartment $job
 * @property Leaves $leave
 * @property SalaryBonus $salary
 */
class Payroll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payroll';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payroll_ID', 'date', 'report', 'total_amount', 'emp_ID', 'job_ID', 'salary_ID', 'leave_ID'], 'required'],
            [['payroll_ID', 'total_amount', 'emp_ID', 'job_ID', 'salary_ID', 'leave_ID'], 'integer'],
            [['date'], 'safe'],
            [['report'], 'string', 'max' => 200],
            [['payroll_ID'], 'unique'],
            [['emp_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['emp_ID' => 'emp_ID']],
            [['job_ID'], 'exist', 'skipOnError' => true, 'targetClass' => JobDepartment::className(), 'targetAttribute' => ['job_ID' => 'job_ID']],
            [['salary_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SalaryBonus::className(), 'targetAttribute' => ['salary_ID' => 'salary_ID']],
            [['leave_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Leaves::className(), 'targetAttribute' => ['leave_ID' => 'leave_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payroll_ID' => 'Payroll ID',
            'date' => 'Date',
            'report' => 'Report',
            'total_amount' => 'Total Amount',
            'emp_ID' => 'Employee ID',
            'job_ID' => 'Job ID',
            'salary_ID' => 'Salary ID',
            'leave_ID' => 'Leave ID',
        ];
    }

    /**
     * Gets query for [[Emp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmp()
    {
        return $this->hasOne(Employee::className(), ['emp_ID' => 'emp_ID']);
    }

    /**
     * Gets query for [[Job]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(JobDepartment::className(), ['job_ID' => 'job_ID']);
    }

    /**
     * Gets query for [[Leave]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeave()
    {
        return $this->hasOne(Leaves::className(), ['leave_ID' => 'leave_ID']);
    }

    /**
     * Gets query for [[Salary]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalary()
    {
        return $this->hasOne(SalaryBonus::className(), ['salary_ID' => 'salary_ID']);
    }
}
