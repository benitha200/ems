<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payroll;

/**
 * PayrollSearch represents the model behind the search form of `app\models\Payroll`.
 */
class PayrollSearch extends Payroll
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payroll_ID', 'total_amount', 'emp_ID', 'job_ID', 'salary_ID', 'leave_ID'], 'integer'],
            [['date', 'report'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payroll::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payroll_ID' => $this->payroll_ID,
            'date' => $this->date,
            'total_amount' => $this->total_amount,
            'emp_ID' => $this->emp_ID,
            'job_ID' => $this->job_ID,
            'salary_ID' => $this->salary_ID,
            'leave_ID' => $this->leave_ID,
        ]);

        $query->andFilterWhere(['like', 'report', $this->report]);

        return $dataProvider;
    }
}
