<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manager".
 *
 * @property int $manager_ID
 * @property string $name
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string|null $authKey
 */
class Manager extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'username', 'phone_number', 'password'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['username'], 'string', 'max' => 80],
            [['phone_number'], 'string', 'max' => 20],
            [['password', 'authKey'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manager_ID' => 'Manager ID',
            'name' => 'Name',
            'username' => 'Username',
            'phone_number' => 'Phone Number',
            'password' => 'Password',
            'authKey' => 'Auth Key',
        ];
    }
    // public function beforeSave($insert)
    // {
    //     if ($this->scenario=='register') {
    //         $this->generateHash();
    //     } elseif (!empty($this->password)) {
    //         $this->generateHash();
    //     }

    //     return parent::beforeSave($insert);
    // }
    // public function generateHash()
    // {
    //     $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
    // }
    public function getAuthKey(){
        return $this->authKey;
    }
    public function getId(){
        return $this->manager_ID;
    }
    public function validateAuthKey($authKey){
        return $this->authKey===$authKey;
    }
    public static function findIdentity($id){
        return self::findOne($id);
    }
    public static function findIdentityByAccessToken($token,$type=null){
        throw new \yii2\base\NotSupportedException;
    }
    public static function findByUsername($username){
        return self::findOne(['username'=>$username]);
    }
    public function validatePassword($password){
       return $this->password ===$password;

       // return Yii::$app->getSecurity()->generatePasswordHash( $password);
    }
  
}
