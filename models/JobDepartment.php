<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "job_department".
 *
 * @property int $job_ID
 * @property string $job_dept
 * @property string $name
 * @property string $description
 * @property int $salary_range
 *
 * @property Payroll[] $payrolls
 * @property SalaryBonus[] $salaryBonuses
 */
class JobDepartment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_dept', 'name', 'description', 'salary_range'], 'required'],
            [['job_ID', 'salary_range'], 'string'],
            [['job_dept', 'name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 200],
            [['job_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_ID' => 'Job ID',
            'job_dept' => 'Job Department',
            'name' => 'Name',
            'description' => 'Description',
            'salary_range' => 'Salary Range',
        ];
    }

    /**
     * Gets query for [[Payrolls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayrolls()
    {
        return $this->hasMany(Payroll::className(), ['job_ID' => 'job_ID']);
    }

    /**
     * Gets query for [[SalaryBonuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalaryBonuses()
    {
        return $this->hasMany(SalaryBonus::className(), ['job_ID' => 'job_ID']);
    }
}
