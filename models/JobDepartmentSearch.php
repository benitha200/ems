<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JobDepartment;

/**
 * JobDepartmentSearch represents the model behind the search form of `app\models\JobDepartment`.
 */
class JobDepartmentSearch extends JobDepartment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_ID', 'salary_range'], 'integer'],
            [['job_dept', 'name', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobDepartment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_ID' => $this->job_ID,
            'salary_range' => $this->salary_range,
        ]);

        $query->andFilterWhere(['like', 'job_dept', $this->job_dept])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
