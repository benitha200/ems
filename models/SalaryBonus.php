<?php

namespace app\models;
use app\models\JobDepartment;

use Yii;

/**
 * This is the model class for table "salary_bonus".
 *
 * @property int $salary_ID
 * @property int $amount
 * @property int $anual
 * @property int $bonus
 * @property int $job_ID
 *
 * @property JobDepartment $job
 * @property Payroll[] $payrolls
 */
class SalaryBonus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salary_bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        
        return [
            [['amount', 'anual', 'bonus', 'job_ID'], 'required'],
            [['salary_ID', 'amount', 'anual', 'bonus', 'job_ID'], 'integer'],
            [['salary_ID'], 'unique'],
            [['job_ID'], 'exist', 'skipOnError' => true, 'targetClass' => JobDepartment::className(), 'targetAttribute' => ['job_ID' => 'job_ID']],
        ];
       
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'salary_ID' => 'Salary ID',
            'amount' => 'Amount',
            'anual' => 'Anual',
            'bonus' => 'Bonus',
            'job_ID' => 'Job ID',
        ];
    }

    /**
     * Gets query for [[Job]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(JobDepartment::className(), ['job_ID' => 'job_ID']);
    }

    /**
     * Gets query for [[Payrolls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayrolls()
    {
        return $this->hasMany(Payroll::className(), ['salary_ID' => 'salary_ID']);
    }
}
