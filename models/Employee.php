<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $emp_ID
 * @property string $fname
 * @property string $lname
 * @property string $gender
 * @property int $age
 * @property string $contact_add
 * @property string $emp_email
 * @property string $emp_pass
 *
 * @property Leaves[] $leaves
 * @property Payroll[] $payrolls
 * @property Qualification[] $qualifications
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fname', 'lname', 'gender', 'age', 'contact_add', 'emp_email', 'emp_pass'], 'required'],
            [['emp_ID', 'age'], 'integer'],
            [['fname', 'lname'], 'string', 'max' => 60],
            [['gender'], 'string', 'max' => 10],
            [['contact_add'], 'string', 'max' => 20],
            [['emp_email'], 'string', 'max' => 50],
            [['emp_pass'], 'string', 'max' => 100],
            [['emp_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_ID' => 'ID',
            'fname' => 'First name',
            'lname' => 'Last name',
            'gender' => 'Gender',
            'age' => 'Age',
            'contact_add' => 'Contact Add',
            'emp_email' => 'Employee Email',
            'emp_pass' => 'Employee Password',
        ];
    }

    /**
     * Gets query for [[Leaves]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeaves()
    {
        return $this->hasMany(Leaves::className(), ['emp_ID' => 'emp_ID']);
    }

    /**
     * Gets query for [[Payrolls]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayrolls()
    {
        return $this->hasMany(Payroll::className(), ['emp_ID' => 'emp_ID']);
    }

    /**
     * Gets query for [[Qualifications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQualifications()
    {
        return $this->hasMany(Qualification::className(), ['emp_ID' => 'emp_ID']);
    }
}
