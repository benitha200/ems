<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Qualification;

/**
 * QualificationSearch represents the model behind the search form of `app\models\Qualification`.
 */
class QualificationSearch extends Qualification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qual_ID', 'emp_ID'], 'integer'],
            [['position', 'requirements', 'date_in'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Qualification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qual_ID' => $this->qual_ID,
            'date_in' => $this->date_in,
            'emp_ID' => $this->emp_ID,
        ]);

        $query->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'requirements', $this->requirements]);

        return $dataProvider;
    }
}
