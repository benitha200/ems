<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <!-- <p>
        <?= Html::a('Create Employee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['label'=>'numero',
            'attribute'=>'emp_ID',
        ],
            'fname',
            'lname',
            'gender',
            'age',
            //'contact_add',
            //'emp_email:email',
            //'emp_pass',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\Employee $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_ID' => $model->emp_ID]);

                 }
            ],
    ],
    ]); ?>-->

   <?=
    GridView::widget([
    'id' => 'kv-grid-demo',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    //'columns' => $gridColumns, // check this value by clicking GRID COLUMNS SETUP button at top of the page
    'headerContainer' => ['style' => 'top:50px', 'class' => 'kv-table-header'], // offset from top
    'floatHeader' => true, // table header floats when you scroll
    'floatPageSummary' => true, // table page summary floats when you scroll
    'floatFooter' => false, // disable floating of table footer
    'pjax' => false, // pjax is set to always false for this demo
    // parameters from the demo form
    'responsive' => false,
    'bordered' => true,
    'striped' => false,
    'condensed' => true,
    'hover' => true,
    'showPageSummary' => true,
    //'panel' => [
       // 'after' => '<div class="float-right float-end"><button type="button" class="btn btn-primary" onclick="var keys = $("#kv-grid-demo").yiiGridView("getSelectedRows").length; alert(keys > 0 ? "Downloaded " + keys + " selected books to your account." : "No rows selected for download.");"><i class="fas fa-download"></i> Download Selected</button></div><div class="clearfix"></div>',
       // 'heading' => '<i class="fas fa-book" icon="book"></i>  Employee',
       // 'type' => 'primary',
        //'before' => '<div style="padding-top: 7px;"><em>* Resize table columns just like a spreadsheet by dragging the column edges.</em></div>',
    //],

     'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="fas fa-globe"></i> Employees</h3>',
        'type'=>'success',
        'before'=>Html::a('<i class="fas fa-plus"></i> Add Employee', ['create'], ['class' => 'btn btn-info']),
        //'after'=>Html::a('<i class="fas fa-redo"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false],

    'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            ['label'=>'numero',
            'attribute'=>'emp_ID',
        ],
            'fname',
            'lname',
            'gender',
            'age',
            'contact_add',
            'emp_email:email',
            //'emp_pass',
            [
                //'label'=>'Actions',
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\Employee $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_ID' => $model->emp_ID]);
                 }

            ]

        ],
    // set export properties
    'export' => [
        'fontAwesome' => true
    ],
    // 'exportConfig' => [
    //     'html' => [],
    //     'csv' => [],
    //     'txt' => [],
    //     'xls' => [],
    //     'pdf' => [],
    //     'json' => [],
    // ],

    'exportConfig'=> [
        GridView::EXCEL => ['filename' =>'Employee list'],
        GridView::PDF => ['filename' =>'Employee list'],
        ],


    // set your toolbar
    'toolbar' =>  [
        [
            'content' =>
                
                Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
                    'class' => 'btn btn-outline-secondary',
                    'title'=>Yii::t('kvgrid', 'Reset Grid'),
                    'data-pjax' => 0, 
                ]), 
            'options' => ['class' => 'btn-group mr-2 me-2']
        ],
        '{export}',
        '{toggleData}',
    ],
    'toggleDataContainer' => ['class' => 'btn-group mr-2 me-2'],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
    'itemLabelSingle' => 'employee',
    'itemLabelPlural' => 'employees'
],
    );
     ?>






</div>
