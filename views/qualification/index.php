<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QualificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qualifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qualification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Qualification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'qual_ID',
            'position',
            'requirements',
            'date_in',
            //'emp_ID',
            [
                 'attribute'=>'emp_ID',
                 'value'=>'employee.field_gender'
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\Qualification $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'qual_ID' => $model->qual_ID]);
                 }
            ],
        ],
    ]); ?>


</div>
