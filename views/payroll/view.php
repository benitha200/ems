<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payroll */

$this->title = $model->payroll_ID;
$this->params['breadcrumbs'][] = ['label' => 'Payrolls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payroll-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'payroll_ID' => $model->payroll_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'payroll_ID' => $model->payroll_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'payroll_ID',
            'date',
            'report',
            'total_amount',
            'emp_ID',
            'job_ID',
            'salary_ID',
            'leave_ID',
        ],
    ]) ?>

</div>
