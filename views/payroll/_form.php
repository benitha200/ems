<?php



use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Employee;
use app\models\JobDepartment;
use app\models\SalaryBonus;
use app\models\Leaves;



/* @var $this yii\web\View */
/* @var $model app\models\Payroll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payroll-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payroll_ID')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'report')->textarea(['rows' => 5]) ?>

    <?= $form->field($model, 'total_amount')->textInput() ?>

     <?= $form->field($model,'emp_ID')->dropDownList(
        ArrayHelper::map(Employee::find()->all(),'emp_ID','lname','emp_ID'),['prompt'=>'Select employee ID']
 ) ?>              

<?= $form->field($model,'job_ID')->dropDownList(
        ArrayHelper::map(JobDepartment::find()->all(),'job_ID','name'),['prompt'=>'Select employee ID']
 ) ?> 

   <?= $form->field($model,'salary_ID')->dropDownList(
    ArrayHelper::map(SalaryBonus::find()->all(),'salary_ID','salary_ID'),['prompt'=>'select salary Id']
   )?>
   <?= $form->field($model,'leave_ID')->dropDownList(
    ArrayHelper::map(Leaves::find()->all(),'leave_ID','leave_ID'),['prompt'=>'select leave ID']
   )?>

    <!--<?= $form->field($model, 'job_ID')->textInput() ?>-->

    <!--<?= $form->field($model, 'salary_ID')->textInput() ?>-->

   <!-- <?= $form->field($model, 'leave_ID')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
