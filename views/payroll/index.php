<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PayrollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payrolls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payroll', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'payroll_ID',
            'date',
            'report',
            'total_amount',
            'emp_ID',
            //'job_ID',
            //'salary_ID',
            //'leave_ID',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, app\models\Payroll $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'payroll_ID' => $model->payroll_ID]);
                 }
            ],
        ],
    ]); ?>


</div>
