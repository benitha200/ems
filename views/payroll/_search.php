<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PayrollSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payroll-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'payroll_ID') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'report') ?>

    <?= $form->field($model, 'total_amount') ?>

    <?= $form->field($model, 'emp_ID') ?>

    <?php // echo $form->field($model, 'job_ID') ?>

    <?php // echo $form->field($model, 'salary_ID') ?>

    <?php // echo $form->field($model, 'leave_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
