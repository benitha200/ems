<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobDepartment */

$this->title = 'Update Job Department: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Job Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'job_ID' => $model->job_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-department-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
