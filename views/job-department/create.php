<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobDepartment */

$this->title = 'Create Job Department';
$this->params['breadcrumbs'][] = ['label' => 'Job Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-department-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
