<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JobDepartment */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Job Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="job-department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'job_ID' => $model->job_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'job_ID' => $model->job_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'job_ID',
            'job_dept',
            'name',
            'description',
            'salary_range',
        ],
    ]) ?>

</div>
