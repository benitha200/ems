<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JobDepartment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-department-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'job_ID')->textInput() ?>-->

    <?= $form->field($model, 'job_dept')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'salary_range')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
