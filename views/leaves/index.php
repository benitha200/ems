<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeavesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leaves';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leaves-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Leaves', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'leave_ID',
            'date',
            'reason',
            'emp_ID',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\Leaves $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'leave_ID' => $model->leave_ID]);
                 }
            ],
        ],
    ]); ?>


</div>
