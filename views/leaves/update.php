<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Leaves */

$this->title = 'Update Leaves: ' . $model->leave_ID;
$this->params['breadcrumbs'][] = ['label' => 'Leaves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->leave_ID, 'url' => ['view', 'leave_ID' => $model->leave_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="leaves-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
