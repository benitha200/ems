<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Employee;

/* @var $this yii\web\View */
/* @var $model app\models\Leaves */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leaves-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'leave_ID')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'reason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emp_ID')->dropDownList(
ArrayHelper::map(Employee::find()->all(),'emp_ID','emp_ID'),['prompt'=>'enter employee ID']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
