<?php

use yii\helpers\Html;
use app\models\JobDepartment;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\SalaryBonus */

$this->title = 'Create Salary Bonus';
$this->params['breadcrumbs'][] = ['label' => 'Salary Bonuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salary-bonus-create">

    <h1><?= Html::encode($this->title) ?></h1>
 <?php $conditions=JobDepartment::find()->all(); ?>
    <?= $this->render('_form', [
        'model' => $model,
       
    ]) ?>

</div>
