<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SalaryBonus */

$this->title = 'Update Salary Bonus: ' . $model->salary_ID;
$this->params['breadcrumbs'][] = ['label' => 'Salary Bonuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->salary_ID, 'url' => ['view', 'salary_ID' => $model->salary_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="salary-bonus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
