<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SalaryBonusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Salary Bonuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salary-bonus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Salary Bonus', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'salary_ID',
            'amount',
            'anual',
            'bonus',
            'job_ID',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\SalaryBonus $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'salary_ID' => $model->salary_ID]);
                 }
            ],
        ],
    ]); ?>


</div>
