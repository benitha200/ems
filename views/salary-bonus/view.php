<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SalaryBonus */

$this->title = $model->salary_ID;
$this->params['breadcrumbs'][] = ['label' => 'Salary Bonuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="salary-bonus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'salary_ID' => $model->salary_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'salary_ID' => $model->salary_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'salary_ID',
            'amount',
            'anual',
            'bonus',
            'job_ID',
        ],
    ]) ?>

</div>
