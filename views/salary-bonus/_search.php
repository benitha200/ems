<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SalaryBonusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salary-bonus-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'salary_ID') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'anual') ?>

    <?= $form->field($model, 'bonus') ?>

    <?= $form->field($model, 'job_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
