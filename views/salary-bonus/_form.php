<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SalaryBonus;
use app\models\JobDepartment;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SalaryBonus */
/* @var $form yii\widgets\ActiveForm */
$conditions=['New'=>'New','Old'=>'Old','speial'=>'special'];
?>

<div class="salary-bonus-form">

    <?php $form = ActiveForm::begin(); ?>

   <!-- <?= $form->field($model, 'salary_ID')->textInput() ?> -->

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'anual')->textInput() ?>

    <?= $form->field($model, 'bonus')->textInput() ?>
    <?=$form->field($model,'job_ID')->dropDownList(
        ArrayHelper::map(JobDepartment::find()->all(),'job_ID','name'),['prompt'=>'enter job_ID']
    )?>

  <!--  <?= $form->field($model, 'job_ID')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
