<?php

namespace app\controllers;

use app\models\Leaves;
use app\models\LeavesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LeavesController implements the CRUD actions for Leaves model.
 */
class LeavesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Leaves models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LeavesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Leaves model.
     * @param int $leave_ID Leave ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($leave_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($leave_ID),
        ]);
    }

    /**
     * Creates a new Leaves model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Leaves();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'leave_ID' => $model->leave_ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Leaves model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $leave_ID Leave ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($leave_ID)
    {
        $model = $this->findModel($leave_ID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'leave_ID' => $model->leave_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Leaves model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $leave_ID Leave ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($leave_ID)
    {
        $this->findModel($leave_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Leaves model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $leave_ID Leave ID
     * @return Leaves the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($leave_ID)
    {
        if (($model = Leaves::findOne(['leave_ID' => $leave_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
