<?php

namespace app\controllers;

use app\models\JobDepartment;
use app\models\JobDepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JobDepartmentController implements the CRUD actions for JobDepartment model.
 */
class JobDepartmentController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all JobDepartment models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new JobDepartmentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JobDepartment model.
     * @param int $job_ID Job ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($job_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($job_ID),
        ]);
    }

    /**
     * Creates a new JobDepartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new JobDepartment();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'job_ID' => $model->job_ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing JobDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $job_ID Job ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($job_ID)
    {
        $model = $this->findModel($job_ID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'job_ID' => $model->job_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JobDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $job_ID Job ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($job_ID)
    {
        $this->findModel($job_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JobDepartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $job_ID Job ID
     * @return JobDepartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($job_ID)
    {
        if (($model = JobDepartment::findOne(['job_ID' => $job_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
