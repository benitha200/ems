<?php

namespace app\controllers;

use app\models\Manager;
use app\models\ManagerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManagerController implements the CRUD actions for Manager model.
 */
class ManagerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Manager models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ManagerSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Manager model.
     * @param int $manager_ID Manager ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($manager_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($manager_ID),
        ]);
    }

    /**
     * Creates a new Manager model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Manager();
        
        if ($this->request->isPost) {
            $model->attributes=$_POST['Manager'];

            $password=$models->password;
           
           //\Yii::$app->security->generatePasswordHash($password);
            if ($model->load($this->request->post()) && $model->save()) {

            // \Yii::$app->security->generatePasswordHash($password);
                return $this->redirect(['view', 'manager_ID' => $model->manager_ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }



//      public function actionCreate()
//     {
//          $model=new Manager();
        
//         if ($this->request->isPost) {

//          if(isset($_POST['Manager']))
//          {
//               $model->attributes=$_POST['Manager'];
//               $model->password=crypt($model->password,'salt');
//               if($model->save())
//                 $this->redirect(array('view','manager_ID'=>$model->manager_ID));
//          }
//          $this->render('create',array(
//              'model'=>$model,
//          ));
//     }
// }

    /**
     * Updates an existing Manager model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $manager_ID Manager ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($manager_ID)
    {
        $model = $this->findModel($manager_ID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'manager_ID' => $model->manager_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Manager model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $manager_ID Manager ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($manager_ID)
    {
        $this->findModel($manager_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Manager model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $manager_ID Manager ID
     * @return Manager the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($manager_ID)
    {
        if (($model = Manager::findOne(['manager_ID' => $manager_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
