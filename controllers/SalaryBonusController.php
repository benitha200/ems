<?php

namespace app\controllers;

use app\models\SalaryBonus;
use app\models\SalaryBonusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalaryBonusController implements the CRUD actions for SalaryBonus model.
 */
class SalaryBonusController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all SalaryBonus models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SalaryBonusSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SalaryBonus model.
     * @param int $salary_ID Salary ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($salary_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($salary_ID),
        ]);
    }

    /**
     * Creates a new SalaryBonus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new SalaryBonus();

        

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'salary_ID' => $model->salary_ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SalaryBonus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $salary_ID Salary ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($salary_ID)
    {
        $model = $this->findModel($salary_ID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'salary_ID' => $model->salary_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SalaryBonus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $salary_ID Salary ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($salary_ID)
    {
        $this->findModel($salary_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SalaryBonus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $salary_ID Salary ID
     * @return SalaryBonus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($salary_ID)
    {
        if (($model = SalaryBonus::findOne(['salary_ID' => $salary_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
